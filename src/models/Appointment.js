import mongoose from 'mongoose';

const appointSchema = new mongoose.Schema({
  startTime: {
    type: Date,
    required: true
  },
  userId: {
    type: mongoose.ObjectId,
    required: true
  },
  description: {
    type: String,
    requires: true
  }
});

const Appoint = mongoose.model('Appointment', appointSchema);

module.exports = Appoint;