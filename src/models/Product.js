import mongoose from 'mongoose';

const productSchema = new mongoose.Schema({
  prodName: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  discount: {
    type: Number,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  stock: {
    type: Number,
    required: true
  },
  bockedStock:{
    type: Number,
    default: 0
  },
  featured:{
    type: Boolean,
    default: false
  }
});

const Product = mongoose.model('Product', productSchema);

module.exports = Product;
