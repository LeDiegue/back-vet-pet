import express from "express";
var router = express.Router();
import Product from "../models/Product";
import Featured from "../models/Featured"
import checkRole from "../middelwares/AuthRole";
import AuthToken from "../middelwares/AuthToken";


router.get("/search", (req, res) => {
  const page = parseInt(req.query.page);
  const quantity = parseInt(req.query.quantity);
  if (page < 0 || isNaN(page) || quantity < 1 || isNaN(quantity)) {
    res.status(503).send({ msg: "parametros de la consulta, inválidos" });
    return;
  }
  Product.find({}, null, { skip: page * quantity, limit: quantity }, function (error, prods) {
    if (error) {
      res.send(err);
      return;
    }
    if (prods === null) {
      res.status(500).json({ msg: "No hay productos en stock" });
      return;
    }
    Product.estimatedDocumentCount({}, (err, pages) => {
      if (err) {
        res.status(500).send(err)
        return;
      }
      console.log("TCL: prods", prods.length)
      res.send({ prods: prods, totalpages: Math.ceil(pages / quantity) });
    })
  })
});

router.get("/searchAll", (req, res) => {
  
  Product.find({}, null, function (error, prods) {
    if (error) {
      res.send(err);
      return;
    }
    if (prods === null) {
      res.status(503).json({ msg: "No hay productos en stock" });
      return;
    }
    console.log(prods);
    res.send(prods);
    return;

  })
});


router.post("/addproduct", AuthToken, checkRole({ hasRole: ["admin"] }),(req, res) => {
  Product.findOne({ prodName: req.body.prodName }, function (err, resp) {
    if (err) {
      res.status(422).send({ msg: 'error al registrar el producto' });
      return;
    }
    if (resp !== null) {
      res.status(422).send({ msg: 'El producto que desea crear ya existe' });
      return;
    }
    const newProduct = new Product({
      prodName: req.body.prodName.toLowerCase(),
      image: req.body.base64,
      discount: req.body.discount,
      price: req.body.price,
      stock: req.body.stock,
      featured: req.body.featured
    });
    newProduct.save((error, data) => {
      if (error) {
        res.status(422).send({ msg: 'error al registrar el producto' });
      } else {
        res.send(data);
        console.log("el producto creado es", data.prodName);
      }
    });
  });
});


// router.get("/search/featured", (req, res) => {

//   Featured.findOne({}, (error, featured) => {
//     if (error) {
//       res.send(error);
//       return;
//     }
//     const prodsId = featured.products.map(prod => prod.prodId)
//     if (prodsId.length) {
//       res.status(503).json({ msg: "No hay productos en stock" });
//       return;
//     }
    
//     Product.find({'_id': { $in: prodsId }}, (error, prods) => {
//       if (error) {
//         res.send(error);
//         return;
//       }
//       if (prods === null) {
//         res.status(503).json({ msg: "No hay productos en stock" });
//         return;
//       }
//       res.send({prods}
//       )
//     })
//   }).sort({created_At: -1})
// })


//Ruta para obtener todos los productos destacados
router.get("/search/allFeatured", (req, res) => {
  Product.find({featured:true}, null , function (error, prods) {
    if (error) {
      res.send(err);
      return;
    }
    if (prods === null) {
      res.status(500).json({ msg: "No hay productos en destacados" });
      return;
    }
    console.log(prods.length);
    res.send(prods);
    // Product.estimatedDocumentCount({}, (err, pages) => {
    //   if (err) {
    //     res.status(500).send(err)
    //     return;
    //   }
    //   console.log("TCL: prods", prods.length)
    // })
  })
});


// router.post("/addfeatured", AuthToken, checkRole({ hasRole: ["admin"] }), (req, res) => {
//   const newFeatured = new Featured({products: req.body.products})
//   newFeatured.save((error, featured) => {
//     if (error) {
//       res.status(502).json({ msg: 'error al registrar destacados' });
//     } else {
//       res.send(featured);
//       console.log("los nuevos destacados son: ", featured.products);
//     }
//   });
// });
  

//ruta para traer datos de un solo producto
router.get("/search/oneproduct/:id", (req, res) => {
  Product.findOne({ _id: req.params.id }, function (error, prod) {
    if (error) {
      res.status(501).send(error);
      return;
    }
    if (prod === null) {
      res.status(500).json({ msg: "No existe este producto" });
      return;
    } else {
      res.send(prod);
    }
  });
});


router.delete('/delete/:id', AuthToken, checkRole({ hasRole: ["admin"] }), (req, res) => {
  Product.findOne({ _id: req.params.id }, function (err, resp) {
    if (err) {
      res.status(502).json({ msg: 'error al borrar producto' });
      return;
    }
    if (resp == null) {
      res.status(521).json({ msg: "No existe este producto" });
      return;
    }
    Product.deleteOne({ _id: req.params.id }, function (err) {
      if (err) {
        res.send(err);
        return;
      }
      res.status(210).json({ msg: "Producto eliminado" });
    });
  })
})


router.put('/modify', AuthToken, checkRole({ hasRole: ["admin"] }), async (req, res) => {
  Product.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, resp) => {
    if (err) {
      res.status(502).json({ msg: 'error al modificar el producto' });
      return;
    }
    if (resp == null) {
      res.status(502).json({ msg: "No existe este producto" });
      return;
    }
    res.send(resp);
  })
})

export default router;
