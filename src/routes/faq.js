import express from 'express';
var router = express.Router();
import Faq from '../models/Faq';


// Ruta para cargar la consulta
router.post('/addfaq', async (req, res, next) => {
  const newFaq = new Faq(req.body);
  newFaq.save((err, data) => {
    if (err) {
      res.status(500).send({msg:"Hubo un error al guardar la consulta" ,err});
    } else {
      res.send(data);
    }
  });
});


//Obtener la lista de todas las consultas
router.get('/getallfaq', async (req, res, next) => {
  Faq.find({}, async (err, Faq) => {
    if (err) {
      res.status(500).send({msg:"Hubo un error al cargar las consultas" ,err});
      return;
    }
    res.send({ Faqs: Faq })
  })
});


//Obtener una sola consulta por id
router.get('/:id', (req, res, next) => {
  Faq.findById(req.params.id, (err, faq) => {
    if (err) {
      res.status(500).send({msg:"No se pudo encontrar la consulta" ,err});
      return
    }
    if (faq===null) {
      res.status(500).send({msg:"No se pudo encontrar la consulta"});
      return
    }
    res.send(faq);
  })
});


//Para modificar una consulta
router.put('/replyfaq/:id', (req, res) => {
  Faq.findByIdAndUpdate(req.params.id, {reply: req.body.reply, answeredState: true},{useFindAndModify : false}, (err, faq) => {
    if (err) {
      res.status(500).send({msg:"No se pudo encontrar la consulta" ,err});
      return
    }
    if (faq===null) {
      res.status(500).send({msg:"No se pudo encontrar la consulta"});
      return
    }
    res.send(faq);
  })
});

export default router;
